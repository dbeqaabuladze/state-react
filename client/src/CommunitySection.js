import React, { useEffect, useState } from "react";

const CommunitySection = () => {
  const [communityData, setCommunityData] = useState([]);
  const [performanceMetrics, setPerformanceMetrics] = useState(null);
  const [isVisible, setIsVisible] = useState(true);

  useEffect(() => {
    const startTime = performance.now();
    fetch("http://localhost:3000/community")
      .then((response) => response.json())
      .then((data) => {
        const endTime = performance.now();
        const duration = endTime - startTime;

        console.log(`Fetching /community took ${duration} milliseconds`);

        setPerformanceMetrics({
          eventType: "fetchCommunityData",
          startTime,
          endTime,
          duration,
        });

        setCommunityData(data);
      })
      .catch((error) => {
        console.error("Error fetching community data:", error);
      });
  }, []);

  const toggleVisibility = () => {
    setIsVisible(!isVisible);
  };

  useEffect(() => {
    if (performanceMetrics) {
      // Send performanceMetrics to the worker or perform any other action
      // sendMessageToWorker("performanceMetrics", performanceMetrics);
    }
  }, [performanceMetrics]);

  return (
    <section className="app-section app-section--community">
      <h2 className="app-title">
        Big Community of <br /> People Like You
      </h2>
      <button className="hide-section" onClick={toggleVisibility}>
        {isVisible ? "Hide section" : "Show section"}
      </button>
      {isVisible && (
        <>
          <h3 className="app-subtitle">
            We’re proud of our products, and we’re really excited
            <br /> when we get feedback from our users.
          </h3>
          <div className="testimonials-container">
            {communityData.map((person) => (
              <div key={person.id} className="testimonial">
                <img
                  className="testimonial-img"
                  src={person.avatar}
                  alt={`${person.firstName} ${person.lastName}`}
                />
                <p className="testimonial-text">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolor.
                </p>
                <div className="testimonial-author">
                  <div className="testimonial-author-firstname">
                    {person.firstName}
                  </div>
                  <div className="testimonial-author-lastname">
                    {person.lastName}
                  </div>
                </div>
                <div className="testimonial-position">{person.position}</div>
              </div>
            ))}
          </div>
        </>
      )}
    </section>
  );
};

export default CommunitySection;
