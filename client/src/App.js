import "./index.css";
import JoinUsSection from "./joinOurProgramSection";
import CommunitySection from "./CommunitySection";

function App() {
  return (
    <div>
      <JoinUsSection />
      <CommunitySection />
    </div>
  );
}

export default App;
